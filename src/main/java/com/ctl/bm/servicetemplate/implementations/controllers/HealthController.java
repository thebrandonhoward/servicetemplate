package com.ctl.bm.servicetemplate.implementations.controllers;

import com.ctl.bm.servicetemplate.collaborators.boundaries.Monitor;

public class HealthController {

    String dispatchPing(Monitor monitor){
        return monitor.ping();
    }

}
