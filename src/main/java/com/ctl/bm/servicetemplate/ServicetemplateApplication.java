package com.ctl.bm.servicetemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicetemplateApplication {

    public static void main(String[] args) {

        SpringApplication.run(ServicetemplateApplication.class, args);

    }

}
