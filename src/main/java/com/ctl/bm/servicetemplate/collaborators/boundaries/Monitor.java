package com.ctl.bm.servicetemplate.collaborators.boundaries;

public interface Monitor {
    String ping();
}
