package com.ctl.bm.servicetemplate.collaborators.models;

import com.ctl.bm.servicetemplate.collaborators.boundaries.Monitor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HeartbeatMonitor implements Monitor {
    @Override
    public String ping() {
        log.info("Ping {}", HeartbeatMonitor.class.getCanonicalName());
        return "OK";
    }
}
