package com.ctl.bm.servicetemplate.collaborators.models;

import com.ctl.bm.servicetemplate.collaborators.boundaries.Monitor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DefaultMonitor implements Monitor {
    @Override
    public String ping() {
        log.info("Ping {}", DefaultMonitor.class.getCanonicalName());
        return "OK";
    }
}
