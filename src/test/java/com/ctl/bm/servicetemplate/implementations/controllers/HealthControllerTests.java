package com.ctl.bm.servicetemplate.implementations.controllers;

import com.ctl.bm.servicetemplate.collaborators.boundaries.Monitor;
import com.ctl.bm.servicetemplate.collaborators.models.DefaultMonitor;
import com.ctl.bm.servicetemplate.collaborators.models.HeartbeatMonitor;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import static org.junit.jupiter.api.Assertions.*;

import org.springframework.boot.test.context.SpringBootTest;

@RunWith(Arquillian.class)
//@SpringBootTest
class HealthControllerTests {

    @Deployment(name = "HealthDeployment_1", order = 1)
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                         .addClass(HealthController.class)
                         .addClass(Monitor.class)
                         .addClass(DefaultMonitor.class)
                         .addClass(HeartbeatMonitor.class)
                         .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    @OperateOnDeployment("HealthDeployment_1")
    void thatPingsWorkForEachMonitorType() {
        HealthController heartbeatController = new HealthController();

        Monitor monitor = new DefaultMonitor();
        assertEquals("OK", heartbeatController.dispatchPing(monitor));

        monitor = new HeartbeatMonitor();
        assertEquals("OK", heartbeatController.dispatchPing(monitor));
    }

}
