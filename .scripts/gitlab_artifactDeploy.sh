#!/bin/sh

echo "Starting artifact deploy process...."

# get current version to variable
currentVersion=`mvn help:evaluate -Dexpression=project.version -q -DforceStdout`

echo "Deploying $currentVersion to artifact repository...";

# remove the echo and replace with real deploy command
echo "mvn -X $MAVEN_CLI_OPTS deploy";

echo "Deploying $currentVersion to artifact repository complete.";

# run a specific gocd stage using the gocd RESTful web service
curl 'http://127.0.0.1:8153/go/api/stages/Build-and-Test-Pipeline/1/build/run' -H 'X-GoCD-Confirm: true' -H 'Accept: application/vnd.go.cd.v1+json' -X POST

echo "Starting artifact deploy process complete.";

exit 0;
