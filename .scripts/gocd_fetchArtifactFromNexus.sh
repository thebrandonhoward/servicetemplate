#!/usr/bin/env bash

artifactUrl='http://nexus-master-idc.idc1.level3.com:8081/nexus/content/groups/public/com/ctl/bm/servicetemplate/1.0.0-SNAPSHOT/servicetemplate-1.0.0-20191114.221509-1.jar'

#Test if artifact exists.
if ( curl -o/dev/null -sfI "$artifactUrl" ); then
  echo "Artifact URL exists."
else
  echo "Artifact URL does not exist."
  exit -1;
fi

#Download artifact from Nexus.
curl -o servicetemplate.jar "$artifactUrl"

